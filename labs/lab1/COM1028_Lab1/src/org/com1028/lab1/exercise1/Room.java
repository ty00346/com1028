package org.com1028.lab1.exercise1;

/**
 * This is a skeleton class which provides an interface
 * 
 * @author css2ht
 *
 */

public abstract class Room {

	protected double area = 0.0;

	public Room(double area) {
     this.area=area;
	}

	public double getArea() {
		return this.area;
	}

}
